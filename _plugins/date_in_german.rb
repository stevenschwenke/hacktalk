# _plugins/date_in_german.rb
require 'i18n'

I18n.load_path += Dir[File.join(File.dirname(__FILE__), 'locales', '*.yml').to_s]
I18n.available_locales = [:de]
I18n.default_locale = :de

module Jekyll
  module DateInGermanFilter
    def date_in_german(input, include_time = true)
      format = include_time ? "%A, %d.%m.%Y, %H:%M" : "%d.%m.%Y"
      I18n.l(input, format: format)
    end
  end
end

Liquid::Template.register_filter(Jekyll::DateInGermanFilter)